[ztsdb](https://hub.docker.com/r/lsilvest/ztsdb/) is a fast, small and
lightweight multi-user noSQL column-store database management system
designed and optimized for the update, storage and handling of
time-series data. Its query and manipulation language is based on the
[R programming language](https://www.r-project.org/) and allows
complex selections of data inside a time-series or across multiple
time-series. It is licensed under GPLv3.

- [home page](http://www.ztsdb.org)
- [download and install](http://www.ztsdb.org/docs/install.html)
- [Docker](https://hub.docker.com/r/lsilvest/ztsdb/)
